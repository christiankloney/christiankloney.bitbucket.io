const gulp 					= require('gulp');
const autoprefixer 	= require('gulp-autoprefixer');
const browserSync 	= require('browser-sync').create(); // Create browser-sync server (browser-sync is not a gulp-plugin, it works with gulp).
const cache 				= require('gulp-cache');
const cssnano 			= require('gulp-cssnano');
const del 					= require('del');
const gulpIf 				= require('gulp-if');
const imagemin 			= require('gulp-imagemin');
const runSequence 	= require('run-sequence');
const sass 					= require('gulp-sass');
const useref 				= require('gulp-useref');
const uglify 				= require('gulp-uglify');


/*********************************************************************************************/
/************************************   DEVELOPMENT TASKS   **********************************/
/*********************************************************************************************/

// browsersync
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
});

gulp.task('clean:dist', function() {
	return del.sync('dist')
});

// sass
gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
		.pipe(sass()) //Uses gulp-sass
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({
      stream: true
    }))
});

/**********************************************************
 ********************* Cache clearing *********************
 *** Image optimization is slow so only clear if needed ***
 **********************************************************/
gulp.task('cache:clear', function (callback) {
	return cache.clearAll(callback)
});

// fonts
gulp.task('fonts', function() {
	return gulp.src('app/fonts/**/*')
	.pipe(gulp.dest('dist/fonts'))
});

// watch
gulp.task('watch', ['browserSync', 'sass'], function() {
	// Watch sass files.
	gulp.watch('app/scss/*.scss', ['sass'])
	// Watch html files.
	gulp.watch('app/*.html', browserSync.reload)
	// Watch js files.
	gulp.watch('app/js/**/*.js', browserSync.reload)
});

// image optimization
gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|gif|svg)')
  .pipe(cache(imagemin({
		//Setting interlaced to true
		interlaced: true,
    optimizationLevel: 10,
	})))
  .pipe(gulp.dest('dist/images'))
});

// ADD AFTER WORKING
gulp.task('autoprefixer', function() {
	return gulp.src('app/css/custom.css')
		.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: true
		}))
		.pipe(gulp.dest('app/css'))
});

// development task - run 'gulp' as task name
gulp.task('dev', function (callback) {
  runSequence(['sass', 'browserSync', 'watch'],
    callback
  )
});

// development task - run 'gulp' as task name
gulp.task('default', ['dev'], function (callback) {
  runSequence(['autoprefixer'],
    callback
  )
});

/*********************************************************************************************/
/************************************   PRODUCTION TASKS   ***********************************/
/*********************************************************************************************/
gulp.task('build', function (callback) {
  runSequence('clean:dist',
    ['sass', 'useref', 'images', 'fonts'],
    callback
  )
})

/**********************************************************
 ********************** Distribution **********************
 ********** Concatenation, minification, uglify  **********
 **********************************************************/
gulp.task('useref', function() {
	return gulp.src('app/*.html')
		.pipe(useref())
		//	Minifies only if it's a Javascript file
		.pipe(gulpIf('*.js', uglify()))
		//	Minifies only if it's a CSS file
		.pipe(gulpIf('*.css', cssnano()))
		.pipe(gulp.dest('dist'))
})

// https://css-tricks.com/gulp-for-beginners/
// Future candidates for this task runner
// https://www.npmjs.com/package/gulp-changed
// https://github.com/webpack/webpack
// https://www.npmjs.com/package/gulp-uncss
// https://www.npmjs.com/package/gulp-csso
// https://www.npmjs.com/package/critical
// https://www.npmjs.com/package/gulp-handlebars
// https://www.npmjs.com/package/gulp-modernizr
// https://www.npmjs.com/package/gulp-sourcemaps
// http://browserify.org/
