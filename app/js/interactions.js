var tintModal = $('#tintModal');
var cleverModal = $('#cleverModal');
var thesisModal = $('#thesisModal');
var closeButton = $('#closeModal');

$('.tint').click(function() {
  console.log('Tint!');
  tintModal.modal('show');
});

$('.designstudio').click(function() {
  console.log('Clever!');
  cleverModal.modal('show');
});

$('.bachelor-thesis').click(function() {
  console.log('Thesis!');
  thesisModal.modal('show');
});

$('#closeTintModal').click(function() {
  console.log('Close Tint!');
  tintModal.modal('hide');
});

$('#closeCleverModal').click(function() {
  console.log('Close Clever!');
  cleverModal.modal('hide');
});

$('#closeThesisModal').click(function() {
  console.log('Close Thesis!');
  thesisModal.modal('hide');
});
